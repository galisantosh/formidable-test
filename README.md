# formidable-test

This is the developer application challenge plugin in wordpress


# Plugin Setup
 
Clone the bitbucket repository or download:

git clone git@bitbucket.org:galisantosh/formidable-test.git

Then, from your WordPress administration panel, go to Plugins > Add New and click the Upload Plugin button at the top of the page.
From your WordPress administration panel go to Plugins > Installed Plugins activate it.


# Configuration

To add shortcode use
[display_test_api_data]


WP Cli command to refresh the data

wp transient delete 