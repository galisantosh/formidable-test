<?php
/**
 * Plugin Name: Formidable Test
 * Plugin URI: https://formidableforms.com
 * Description: This is the Developer Application Challenge Plugin
 * Version: 1.0
 * Author: Santosh Gali
 * Author URI: https://formidableforms.com
 **/




function frontend_admin_ajax_scripts()
{
    wp_enqueue_script('script', plugins_url('/js/script.js', __FILE__), array('jquery'), '1.0', true);
    wp_localize_script('script', 'TestAjax', array(
        // URL to wp-admin/admin-ajax.php to process the request
        'ajaxurl' => admin_url('admin-ajax.php'),
        // generate a nonce with a unique ID 
        // so that you can check it later when an AJAX request is sent
        'security' => wp_create_nonce('my-special-string')
    ));
}
add_action('wp_enqueue_scripts', 'frontend_admin_ajax_scripts');


function test_api_action_javascript()
{
    ?>
    <script>
        jQuery(document).ready(function($) {
            var data = {
                action: 'test_api_action',
            };
            // Since 2.8 ajaxurl is always defined in the admin header and points to admin-ajax.php
            $.get(ajaxurl, data, function(response) {

            });
        });
    </script>
<?php }
add_action('admin_footer', 'test_api_action_javascript');


function test_api_action_callback()
{

    //check_ajax_referer('my-special-string', 'security');
    $cacheData = get_transient('test_api_data');
    if (!$cacheData) {
        $url = 'http://api.strategy11.com/wp-json/challenge/v1/1';
        $response = wp_remote_get($url);
        if (is_wp_error($response))
            return '<p>bad connection!</p>';

        $data = ($response && isset($response['body'])) ? json_decode($response['body']) : '';
        if (!data)
            return '<p>no data found</p>';


        $title = $data->title;
        $tdRows = $thHeaders = '';
        foreach ($data->data->headers as $header) {
            $thHeaders .= "<th> $header </th>";
        }
        foreach ($data->data->rows as $row) {
            $date = date('d-m-Y', $row->date);
            $tdRows .= "<tr> <td> $row->id </td><td> $row->fname </td><td> $row->lname </td><td> $row->email </td><td> $date </td></tr>";
        }
        $responseHtmlFormat = "
            <h4> $title </h4>
            <table> 
                <tr> 
                    $thHeaders
                </tr> 
                $tdRows
            </table>
                            
        ";
        $responseHtmlFormat = apply_filters('dispaly_test_api_data_filter', $responseHtmlFormat);
        set_transient('test_api_data', $responseHtmlFormat, 3600);
    } else {
        $responseHtmlFormat = get_transient('test_api_data');
    }
    echo $responseHtmlFormat;
    die();
}
add_action('wp_ajax_nopriv_test_api_action', 'test_api_action_callback'); //This action calls for non-authenticated users as well
add_action('wp_ajax_test_api_action', 'test_api_action_callback'); //This action calls only for authenticated user

add_shortcode('display_test_api_data', 'test_api_action_callback');
